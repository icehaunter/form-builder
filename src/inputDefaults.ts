import { AnyInput, TextInput, EmailInput, PasswordInput, TextareaInput, FileInput, RadioInput, CheckboxInput } from '@/inputTypes';

export const textInput: TextInput = {
    id: 'default-text',
    type: 'text',
    label: 'Текст',
    required: false,
    readonly: false,
    disabled: false,
    autofocus: false,
    name: 'text',
};

export const passwordInput: PasswordInput = {
    id: 'default-password',
    type: 'password',
    label: 'Пароль',
    required: false,
    readonly: false,
    disabled: false,
    autofocus: false,
    name: 'password',
};

export const emailInput: EmailInput = {
    id: 'default-email',
    type: 'email',
    label: 'Email',
    required: false,
    readonly: false,
    disabled: false,
    autofocus: false,
    name: 'email',
    multiple: false,
};

export const textareaInput: TextareaInput = {
    id: 'default-textarea',
    type: 'textarea',
    label: 'Блок текста',
    required: false,
    readonly: false,
    disabled: false,
    autofocus: false,
    name: 'textarea',
};

export const fileInput: FileInput = {
    id: 'default-file',
    type: 'file',
    label: 'Файл',
    name: 'file',
    accept: '',
    multiple: false,
};

export const radioInput: RadioInput = {
    id: 'default-radio',
    type: 'radio',
    label: 'Выбор варианта',
    name: 'radio',
    children: [],
};

export const checkboxInput: CheckboxInput = {
    id: 'default-checkbox',
    type: 'checkbox',
    label: 'Выбор вариантов',
    name: 'checkbox',
    children: [],
};


export const allInputs: AnyInput[] = [
    textInput,
    passwordInput,
    emailInput,
    textareaInput,
    fileInput,
    radioInput,
    checkboxInput,
];
