export interface FormInput {
    id: string;
    name: string;
    label: string;
}

interface InputBase extends FormInput {
    required?: boolean;
    disabled?: boolean;
    autofocus?: boolean;
    tabindex?: number;
}

interface TextInputBase extends InputBase {
    minLength?: number;
    maxLength?: number;
    pattern?: string;
    placeholder?: string;
    readonly: boolean;
}

export interface TextInput extends TextInputBase {
    type: 'text';
}

export interface PasswordInput extends TextInputBase {
    type: 'password';
}

export interface EmailInput extends TextInputBase {
    type: 'email';
    multiple: boolean;
}

export interface TextareaInput extends TextInputBase {
    type: 'textarea';
}

export interface FileInput extends InputBase {
    type: 'file';
    accept: string;
    multiple: boolean;
    capture?: string;
}

export interface MultipleOption extends InputBase {
    value: string;
    checked?: boolean;
}

export interface RadioInput extends FormInput {
    type: 'radio';
    children: MultipleOption[];
}

export interface CheckboxInput extends FormInput {
    type: 'checkbox';
    children: MultipleOption[];
}

export type AnyMulipleInput = RadioInput | CheckboxInput;

export type AnyInput =
    TextInput | PasswordInput | EmailInput | TextareaInput | FileInput | RadioInput | CheckboxInput;

export type AnyProps = keyof Omit<AnyInput, 'id' | 'type'>;
