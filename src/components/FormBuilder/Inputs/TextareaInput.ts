import Vue from 'vue';
import { TextInput } from '@/inputTypes';


interface IProps {
  input: TextInput;
}

export default Vue.extend<IProps>({
  functional: true,
  render: (h, ctx) => {
    const input = ctx.props.input;

    return h('textarea', {
      ...ctx.data,
      attrs: {
        ...input,
      },
    });
  },
});
