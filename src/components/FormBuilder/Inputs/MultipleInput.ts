import Vue, { VNode } from 'vue';
import { RadioInput } from '@/inputTypes';


interface IProps {
    input: RadioInput;
}

export default Vue.extend<IProps>({
    functional: true,
    render: (h, ctx) => {
        const input = ctx.props.input;

        const children = [].concat.apply(input.children.map((val) => [
            h('input', {
                attrs: {
                    ...val,
                    type: input.type,
                    name: input.name,
                },
            }),
            h('label', {
                attrs: {
                    for: val.id,
                },
            }, val.label),
        ]));

        return h('div', {
            ...ctx.data,
            attrs: {
                id: input.id,
            },
        }, children);
    },
});
