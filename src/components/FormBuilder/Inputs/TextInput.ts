import Vue from 'vue';
import { FormInput } from '@/inputTypes';


interface IProps {
  input: FormInput;
}

export default Vue.extend<IProps>({
  functional: true,
  render: (h, ctx) => {
    const input = ctx.props.input;

    return h('input', {
      ...ctx.data,
      attrs: {
        ...input,
      },
    });
  },
});
