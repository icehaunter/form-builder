import Vue, { VueConstructor, CreateElement, VNode } from 'vue';
import { AnyInput, FormInput } from '@/inputTypes';
import TextInput from '@/components/ElementSelector/TextInput.vue';
import EmailInput from '@/components/ElementSelector/EmailInput.vue';
import PasswordInput from '@/components/ElementSelector/PasswordInput.vue';

interface IProps {
    input: AnyInput;
}

type InputMapping = {
    [key in AnyInput['type']]: (input: FormInput) => VNode | VNode[];
};

export default Vue.extend<IProps>({
    functional: true,
    render: (h, ctx) => {
        const mapping: InputMapping = {
            text: (input) => h('input', {
                attrs: {
                    type: 'text',
                    disabled: true,
                    placeholder: input.label,
                },
            }),
            email: (input) => h('input', {
                attrs: {
                    type: 'email',
                    disabled: true,
                    placeholder: input.label,
                },
            }),
            password: (input) => h('input', {
                attrs: {
                    type: 'password',
                    disabled: true,
                    placeholder: input.label,
                },
            }),
            textarea: (input) => h('textarea', {
                attrs: {
                    disabled: true,
                    placeholder: input.label,
                },
            }),
            file: (input) => h('input', {
                attrs: {
                    type: 'file',
                    disabled: true,
                    placeholder: input.label,
                },
            }),
            radio: (_) => h('div', {}, [
                h('input', {
                    attrs: {
                        type: 'radio',
                        disabled: true,
                        checked: true,
                    },
                }),
                h('label', 'Вариант 1'),
                h('input', {
                    attrs: {
                        type: 'radio',
                        disabled: true,
                        checked: false,
                    },
                }),
                h('label', 'Вариант 2'),
            ]),
            checkbox: (_) => h('div', {}, [
                h('input', {
                    attrs: {
                        type: 'checkbox',
                        disabled: true,
                        checked: true,
                    },
                }),
                h('label', 'Вариант 1'),
                h('input', {
                    attrs: {
                        type: 'checkbox',
                        disabled: true,
                        checked: false,
                    },
                }),
                h('label', 'Вариант 2'),
            ]),
        };

        const component = mapping[ctx.props.input.type];
        return h('div', {
            staticClass: ctx.data.staticClass,
            class: ctx.data.class,
        }, [
                h('label', ctx.props.input.label),
                component(ctx.props.input),
            ]);
    },
});
